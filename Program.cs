﻿using System;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj rozmiar swiata:");
            int rozmiar = Convert.ToInt32(Console.ReadLine()); // pobranie rozmiaru swiata od uzytkownika
            World swiat = new World(rozmiar); // stworzenie swiata do gry
            swiat.Start(); // zainicjowanie swiata losowymi wartosciami komorek
            swiat.Display();
            while (true)
            {
                Console.ReadKey(); // swiat starzeje sie po nacisnieciu dowolnego klawisza
                Console.Clear();
                swiat = swiat.Tick(); // funkcja postarzajaca swiat
                swiat.Display(); // funkcja wyswietlajaca obecny stan swiata
            }
        }
    }

    class World
    {
        private int size; // rozmiar pola gry
        private bool[,] tab; // tablica przechowujaca komorki (true - komorka zyje, false - komorka nie zyje)
        public World(int rozmiar) // konstruktor nadajacy tablicy podane przez uzytkownika wymiary
        {
            size = rozmiar;
            tab = new bool[size, size];
        }
        public void Display() // funkcja wyswietlajaca tablice
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (tab[i, j] == true)
                        Console.Write("* "); // gdy komorka jest zywa
                    else
                        Console.Write(". "); // gdy komorka jest martwa
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
        public void Start() // funkcja generujaca poczatkowe ulozenie
        {
            Random r = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {

                    int rInt = r.Next(0, 2); // losowanie, czy dana komorka jest zywa czy martwa
                    if (rInt == 1)
                        tab[i, j] = true;
                    else
                        tab[i, j] = false;
                }
            }
        }
        private bool Check(int x, int y) // funkcja sprawdzajaca liczbe zyjacych sasiadow
        {
            int livingNeighbours = 0;
            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 1; j <= y + 1; j++)
                {
                    if ((i == x && j == y) || i < 0 || j < 0 || i > (size - 1) || j > (size - 1)) ; // sprawdzenie, czy podane indeksy mieszcza sie w tablicy i czy nie odpowiadaja indeksom badanej komorki
                    else 
                    {
                        if (tab[i, j] == true) // gdy indeksy sa poprawne, sprawdzenie czy sasiadujaca komorka jest zywa
                            livingNeighbours++;
                    }
                }
            }
            if (tab[x, y] == true && livingNeighbours < 2) // gdy komorka jest zywa i ma mniej niz 2 zyjacych sasiadow, to umiera
                return false;
            else if (tab[x, y] == true && livingNeighbours == 2 || livingNeighbours == 3) // gdy komorka jest zywa i ma 2 lub 3 zyjacych sasiadow, to dalej zyje
                return true;
            else if (tab[x, y] == true && livingNeighbours > 3) // gdy komorka jest zywa i ma wiecej niz 3 zyjacych sasiadow to umiera
                return false;
            else if (tab[x, y] == false && livingNeighbours == 3) // gdy komorka jest martwa i ma 3 zyjacych sasiadow, to ozywa
                return true;
            else // w innych przypadkach komorka pozostaje martwa
                return false; 
        }
        public World Tick() // funkcja odpowiadajaca za jedna jednostke czasu
        {
            World swiatTemp = new World(size); // swiat do ktorego beda zapisywane nowe stany komorek
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    swiatTemp.tab[i, j] = Check(i, j); // przypisanie do "nowego" swiata wartosci danej komorki
                }
            }

            return swiatTemp;
        }
    }
}
